angular.module('app').config(['$stateProvider','$urlRouterProvider',
    function($stateProvider,$urlRouterProvider){
	$urlRouterProvider.otherwise('/');
	    $stateProvider.
	    state('top', {
	    	url:'/',
	        controller: 'TopController',
	        templateUrl: 'top.html',
	    }).
	    state('list',{
	    	url:'/list',
	        controller: 'ListController',
	        templateUrl: 'list.html',
	    }).
	    state('list.detail',{
	    	url:'/:customerID',
	        controller: 'CustomerController',
	        templateUrl: 'customer.html',
	    });
}]);