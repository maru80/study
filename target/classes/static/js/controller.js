var app = angular.module('app');

app.controller('AppController', ['$scope','ModalService', function($scope,ModalService) {
	$scope.title = 'Header';
	$scope.open = function() {
		ModalService.openModal({
	        modalHeader : "ヘッダー",
	        modalBody : "ボディ"
	    });
	}
}]);

app.controller('TopController', ['$scope','$filter','CustomerFactory', function($scope,$filter,CustomerFactory) {
	$scope.subtitle = 'Top!!';
}]);

app.controller('ListController', ['$scope','$filter','CustomerFactory', function($scope,$filter,CustomerFactory) {
	$scope.contents = CustomerFactory.query();
	$scope.sort = function(exp,reverse){
		$scope.contents = $filter('orderBy')($scope.contents,exp,reverse);
    }
	$scope.subtitle = 'customer.list';
}]);

app.controller('CustomerController', ['$scope', function($scope) {
	$scope.subtitle = 'customer.detail';
}]);

app.controller('ModalController',['$scope','$modalInstance','modalParam',
  function($scope, $modalInstance, modalParam) {
	 $scope.modalHeader = modalParam.modalHeader;
     $scope.modalBody = modalParam.modalBody;

     $scope.ok = function() {
         $modalInstance.close();
     };
  }
]);