var app = angular.module('app');

app.service("ModalService",["$modal",function($modal) {
    var openModal = function(modalParam) {
        $modal.open(
            {
            	templateUrl: 'modal.html',
    			controller: 'ModalController',
                backdrop : true,
                resolve : {
                    modalParam : function () {
                        return modalParam;
                    }
                }
            }
        );
    };
    return {
        openModal : openModal
    };
}]);

app.factory('CustomerFactory',['$resource','Const', function($resource,Const){
    return $resource(
      Const.baseApiUrl + '/api/customers',
      {},
      {get: {method: 'GET'}}
    );
}]);
