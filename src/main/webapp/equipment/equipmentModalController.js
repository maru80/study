angular
  .module('app')
  .controller('equipmentModalController', equipmentModalController);

equipmentModalController.$inject = ['$modalInstance','modalParam','equipmentFactory', 'utilService'];

function equipmentModalController($modalInstance, modalParam, equipmentFactory, utilService) {
  var vm = this;
  vm.equipment = modalParam.modalObject;
  vm.close = close;
  
  function close(){
	utilService.close($modalInstance);
  }
  
}