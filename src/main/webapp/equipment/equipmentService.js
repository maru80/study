angular
  .module('app')
  .factory('equipmentFactory', equipmentFactory);

equipmentFactory.$inject = ['$resource','constant'];

function equipmentFactory($resource,constant){
  return $resource(
    constant.baseApiUrl + '/equipments/:category',
    {category: '@category'},
    {
      query: {method: 'GET', isArray: true},
	}
  );
};