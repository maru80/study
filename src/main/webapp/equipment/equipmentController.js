angular
  .module('app')
  .controller('equipmentController', equipmentController);

equipmentController.$inject = ['equipmentFactory', 'utilService'];
                                 
function equipmentController(equipmentFactory, utilService) {
  var vm = this;
  vm.equipments = equipmentFactory.query({cid : ''});
  
  vm.category = category;
  vm.open = open;
  
  function category(cate){
      vm.equipments = equipmentFactory.query({category : cate});
  }
  
  function open(index){
    var equipment = vm.equipments[index];
    utilService.createModal('/equipment/modal-equipment.html', 'equipmentModalController as equipmentModal', equipment);
  }
}