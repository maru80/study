angular
  .module('app')
  .controller('loginController', loginController);

loginController.$inject = ['loginService','$http'];

function loginController(loginService, $http){
	var vm = this;
    vm.token = null;
    vm.error = null;
    vm.roleUser = false;
    vm.roleAdmin = false;
    vm.roleFoo = false;

    vm.login = function() {
        vm.error = null;
        loginService.login(vm.userName,vm.userPassword).then(function(token) {
            vm.token = token;
            $http.defaults.headers.common.Authorization = 'Bearer ' + token;
        },
        function(error){
            vm.error = error
            vm.userName = '';
            vm.userPassword = '';
        });
    }

    vm.logout = function() {
        vm.userName = '';
        vm.userPassword = '';
        vm.token = null;
        $http.defaults.headers.common.Authorization = '';
        loginService.destroy();
    }

    vm.loggedIn = function() {
    	return loginService.isLogged();
    }
}