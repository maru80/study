angular
  .module('app')
  .service('loginService', loginService);

loginService.$inject = ['$http'];

function loginService($http){
	var self = this;
	var _token = null;

	this.login = function(username,userpassword) {
        return $http.post('/user/login', {name: username,password: userpassword}).then(function(response) {
        	self.setToken(response.data.token);
            return response.data.token;
        });
    };
    this.setToken = function(token){
        _token = token;
    };
    this.isLogged = function(){
        return !!_token;
    };
    this.destroy = function () {
    	console.log('destroy = ' + _token);
    	_token = null;
    };
}