angular.module('config',[]);

angular.module('config').run([
  '$rootScope',
  '$state',
  'loginService',
  function($rootScope,$state,loginService){
	  console.log('config');
      $rootScope.$on('$stateChangeStart', function(e,toState,toParams,fromState,fromParams) {
          if (!toState.auth) {
              return;
          }

          if(!loginService.isLogged()){
              e.preventDefault();
              $state.go('login');
              }
          });
      }
  ]);