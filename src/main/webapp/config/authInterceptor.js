angular
  .module('app')
  .factory('authInterceptor', authInterceptor);

authInterceptor.$inject = ['$rootScope', '$q', '$window'];

function authInterceptor($rootScope, $q, $window){
	return {
	    request: function (config) {
	      config.headers = config.headers || {};
	      if ($window.sessionStorage.token) {
	        config.headers.Authorization = 'Bearer ' + $window.sessionStorage.token;	        
	      }
	      return config;
	    },
	    response: function (response) {
	      console.log('response.status='+response.status);
	      return response || $q.when(response);
	    },
	    responseError: function(rejection) {
          if (500 == rejection.status) {
            //alert('System Error!');
          }
          return $q.reject(rejection);
        }
	  };
}