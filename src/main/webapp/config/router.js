angular.module('app').config(function ($httpProvider) {
  $httpProvider.interceptors.push('authInterceptor');
});

angular.module('app').config(router);

router.$inject = ['$locationProvider', '$stateProvider', '$urlRouterProvider'];

function router($locationProvider, $stateProvider, $urlRouterProvider){
	$urlRouterProvider.otherwise('/');
	$locationProvider.html5Mode(true);
    
	$stateProvider
		.state('login', {
	    	url:'/login',
	        controller: 'loginController',
	        templateUrl: 'login/login.html',
	    })
	    .state('top', {
	    	url:'/',
	        controller: 'topController',
	        templateUrl: 'top/top.html',
	    })
	    .state('profile',{
	    	url:'/profile',
	        templateUrl: 'profile/profile.html',
	    })
	    .state('equipment',{
	    	url:'/equipment',
	        controller: 'equipmentController',
	        templateUrl: 'equipment/equipment.html',
	    }).state('edit-album', {
	    	url:'/edit-album',
	        controller: 'albumEditController',
	        templateUrl: 'album/edit-album.html',
	    });
}