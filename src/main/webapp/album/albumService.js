angular
  .module('app')
  .factory('albumFactory', albumFactory);

albumFactory.$inject = ['$resource','constant'];

function albumFactory($resource,constant){
  return $resource(
    constant.baseApiUrl + '/albums/:albumid',
    {albumid: '@albumid'},
    {
      get: {
        method: 'GET'
      },
      save: {
        method:'POST'
      },
      delete: {
    	method: 'DELETE'
      },
      update: {
    	method: 'PUT'
      }
    }
  );
};