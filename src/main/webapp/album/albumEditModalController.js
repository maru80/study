angular
  .module('app')
  .controller('albumEditModalController', albumEditModalController);

albumEditModalController.$inject = ['albumFactory','$modalInstance','modalParam','trackFactory', 'utilService'];

function albumEditModalController(albumFactory, $modalInstance, modalParam, trackFactory, utilService) {
  var vm = this;
  vm.album = modalParam.modalObject;
  vm.tracks = (modalParam.modalObject != null) ? trackFactory.query({albumid : modalParam.modalObject.id}) : null ;

  vm.close = close;
  vm.addAlbum = addAlbum;
  vm.updateAlbum =updateAlbum;
  vm.addTrack = addTrack;
  vm.updateTrack = updateTrack;
  vm.deleteTrack = deleteTrack;
  
  function addAlbum(){
	albumFactory.save(vm.album);
  }
  
  function updateAlbum(){
	albumFactory.update({albumid : vm.album.id} ,vm.album);
  }
  
  function addTrack(){
  }
  
  function updateTrack(index){
	var track = vm.tracks[index];
	trackFactory.update({id : track.id} ,track);  
  }
  
  function deleteTrack(index){
	var track = vm.tracks[index];
	console.log(index);
	trackFactory.delete({id : track.id});  
  }

  function close(){
	utilService.close($modalInstance);
  }
}