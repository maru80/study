angular
  .module('app')
  .factory('trackFactory', trackFactory);

trackFactory.$inject = ['$resource','constant'];

function trackFactory($resource,constant){
  return $resource(
    constant.baseApiUrl + '/tracks/:albumid',
    {albumid: '@albumid',id :'@id'},
    {
      query: {
    	method: 'GET', isArray: true},
      delete: {
    	url: constant.baseApiUrl + '/tracks/:id',
        method: 'DELETE'
      },
      update: {
    	url: constant.baseApiUrl + '/tracks/:id',
        method: 'PUT'
      }
    }    
  );
};