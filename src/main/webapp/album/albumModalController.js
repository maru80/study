angular
  .module('app')
  .controller('albumModalController', albumModalController);

albumModalController.$inject = ['$modalInstance','modalParam','trackFactory', 'utilService'];

function albumModalController($modalInstance, modalParam, trackFactory, utilService) {
  var vm = this;
  vm.album = modalParam.modalObject;
  vm.tracks = trackFactory.query({albumid : modalParam.modalObject.id});

  vm.close = close;
  
  function close(){
	utilService.close($modalInstance);
  }
}