angular
  .module('app')
  .controller('albumEditController', albumEditController);

albumEditController.$inject = ['albumFactory','utilService'];

function albumEditController(albumFactory,utilService) {
  var vm = this;
  vm.albums = albumFactory.query();
  vm.addAlbum = addAlbum;
  vm.deleteAlbum = deleteAlbum;
  vm.editAlbum = editAlbum;
  
  function addAlbum(){
	utilService.createModal('/album/modal-addAlbum.html','albumEditModalController as albumEditModal',null);
  }
  
  function deleteAlbum(index){
	var album = vm.albums[index];
    albumFactory.delete({albumid : album.id});
  }
  
  function editAlbum(index){
	var album = vm.albums[index];
	utilService.createModal('/album/modal-editAlbum.html','albumEditModalController as albumEditModal',album);
  }
}