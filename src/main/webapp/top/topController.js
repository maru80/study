angular
  .module('app')
  .controller('topController', topController);

topController.$inject = ['albumFactory','utilService'];

function topController(albumFactory, utilService) {
  var vm = this;
  vm.albums = albumFactory.query();
  vm.open = open;
  
  function open(index){
    var album = vm.albums[index];
    utilService.createModal('/album/modal-album.html','albumModalController as albumModal',album);
  }
}