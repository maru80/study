angular
  .module('app')
  .service('modalService', modalService)
  
modalService.$inject =  ['$modal'];

function modalService($modal) {
  var openModal = function(modalParam) {
    $modal.open(
      {
      	templateUrl: modalParam.modalTemplate,
  		controller: modalParam.modalController,
  		size:'lg',
        backdrop : false,
        resolve : {
          modalParam : function () {
            return modalParam;
          }
        }
      }
    );
  };
  return {
    openModal : openModal
  };
};
