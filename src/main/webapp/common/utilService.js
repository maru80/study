angular
  .module('app')
  .service('utilService', utilService);

utilService.$inject = ['modalService'];

function utilService(modalService) {
  this.createModal = createModal;
  this.close = close;
  
  
  function createModal(paramTemplate,paramController,object) {
    modalService.openModal({
        modalTemplate : paramTemplate,
        modalController : paramController,
        modalObject : object
      });
   }
  
  function close($modalInstance){
	$modalInstance.close();
  }
}