package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.domain.Track;
import com.example.repository.TrackRepository;

@Service
@Transactional
public class TrackService {

	@Autowired
	TrackRepository trackRepository;
		
	public List<Track> findTracks(Integer id) {
        return trackRepository.findTracks(id);
    }
	
	public Track update(Track Track) {
        return trackRepository.save(Track);
    }
	
	public void delete(Integer id) {
		trackRepository.delete(id);
    }
}
