package com.example.service;

import java.util.List;

import com.example.domain.Message;
import com.example.repository.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class MessageService {

	@Autowired
	MessageRepository messageRepository;
	
	public List<Message> findAll() {
        return messageRepository.findAll();
    }
	
	public Message create(Message message) {
        return messageRepository.save(message);
    }
	
	public void delete(Integer id) {
		messageRepository.delete(id);
    }
}
