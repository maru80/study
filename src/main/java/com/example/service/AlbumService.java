package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.domain.Album;
import com.example.repository.AlbumRepository;

@Service
@Transactional
public class AlbumService {

	@Autowired
	AlbumRepository albumRepository;
	
	public List<Album> findAll() {
        return albumRepository.findAll();
    }
	
	public Album findOne(Integer id) {
        return albumRepository.findOne(id);
    }
	
	public Album add(Album album) {
        return albumRepository.save(album);
    }
	
	public Album update(Album album) {
        return albumRepository.save(album);
    }
	
	public void delete(Integer id) {
		albumRepository.delete(id);
    }
}
