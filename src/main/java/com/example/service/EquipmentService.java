package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.example.domain.Equipment;
import com.example.repository.EquipmentRepository;

@Service
@Transactional
public class EquipmentService {

	@Autowired
    EquipmentRepository equipmentRepository;
	
	public List<Equipment> findAll() {
        return equipmentRepository.findAll();
    }
	
	public List<Equipment> findEquipments(String category) {
        return equipmentRepository.findEquipments(category);
    }
}
