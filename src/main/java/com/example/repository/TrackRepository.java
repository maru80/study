package com.example.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.domain.Track;

public interface TrackRepository extends JpaRepository<Track, Integer> {

	@Query("select x from Track x where x.albumId = :albumId order by x.trackNo asc")
    public List<Track> findTracks(@Param("albumId") Integer albumId);
	
}
