package com.example.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.domain.Equipment;

public interface EquipmentRepository extends JpaRepository<Equipment, Integer> {
	
	@Query("select x from Equipment x where x.category = :category order by x.name asc")
    public List<Equipment> findEquipments(@Param("category") String category);

}
