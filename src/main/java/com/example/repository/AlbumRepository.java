package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.domain.Album;

public interface AlbumRepository extends JpaRepository<Album, Integer> {

}
