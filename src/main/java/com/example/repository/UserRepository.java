package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.domain.User;

public interface UserRepository extends JpaRepository<User, Integer> {

	@Query("select x from User x where x.name = :name and x.password = :password")
    public User findUser(@Param("name") String name,@Param("password") String password);
}
