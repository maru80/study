package com.example.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.domain.Equipment;
import com.example.service.EquipmentService;

@RestController
@RequestMapping("api/equipments")
public class EquipmentRestController {

	@Autowired
	EquipmentService equipmentService;
	
	@RequestMapping(method = RequestMethod.GET)
    List<Equipment> getEquipments() {
    	List<Equipment> equipments = equipmentService.findAll();
        return equipments;
    }
	
	@RequestMapping(value = "{category}",method=RequestMethod.GET)
    List<Equipment> getEquipmentsByCategory(@PathVariable String category) {
    	List<Equipment> equipments = equipmentService.findEquipments(category);
        return equipments;
    }
}
