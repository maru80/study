package com.example.api;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.example.domain.Album;
import com.example.service.AlbumService;

@RestController
@RequestMapping("api/albums")
public class AlbumRestController {

	@Autowired
    AlbumService albumService;
	
	@RequestMapping(method = RequestMethod.GET)
    List<Album> getAlbums(@PageableDefault Pageable pageable) {
    	List<Album> albums = albumService.findAll();
        return albums;
    }

    @RequestMapping(value = "{albumid}", method = RequestMethod.GET)
    Album getAlbum(@PathVariable Integer albumid) {
    	Album album = albumService.findOne(albumid);
        return album;
    }
    
    @RequestMapping(method = RequestMethod.POST)
    ResponseEntity<Album> postCustomers(@RequestBody Album album, UriComponentsBuilder uriBuilder) {
        Album created = albumService.add(album);
        URI location = uriBuilder.path("api/albums/{id}")
                .buildAndExpand(created.getId()).toUri();
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(location);
        return new ResponseEntity<>(created, headers, HttpStatus.CREATED);
    }
    
    @RequestMapping(value = "{albumid}", method = RequestMethod.PUT)
    Album putAlbum(@PathVariable Integer albumid, @RequestBody Album album) {
    	album.setId(albumid);
        return albumService.update(album);
    }
    
    @RequestMapping(value = "{albumid}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void deleteAlbum(@PathVariable Integer albumid) {
    	albumService.delete(albumid);
    }
}
