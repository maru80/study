package com.example.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.domain.Track;
import com.example.service.TrackService;

@RestController
@RequestMapping("api/tracks")
public class TrackRestController {

	@Autowired
	TrackService trackService;

	@RequestMapping(value = "{albumid}",method=RequestMethod.GET)
    List<Track> getTracks(@PathVariable Integer albumid) {
    	List<Track> tracks = trackService.findTracks(albumid);
        return tracks;
    }
	
	@RequestMapping(value = "{id}", method = RequestMethod.PUT)
    Track putTrack(@PathVariable Integer id, @RequestBody Track track) {
    	track.setId(id);
        return trackService.update(track);
    }
    
    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void deleteTrack(@PathVariable Integer id) {
    	trackService.delete(id);
    }
}
