package com.example.api;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.example.domain.Message;
import com.example.service.MessageService;

@RestController
@RequestMapping("api/messages")
public class MessageRestController {

	@Autowired
	MessageService messageService;
	
	@RequestMapping(method = RequestMethod.GET)
    List<Message> getMessages(@PageableDefault Pageable pageable) {
    	List<Message> messages = messageService.findAll();
        return messages;
    }

	@RequestMapping(method = RequestMethod.POST)
    ResponseEntity<Message> postmessages(@RequestBody Message message, UriComponentsBuilder uriBuilder) {
		Message created = messageService.create(message);
        URI location = uriBuilder.path("api/messages/{id}")
                .buildAndExpand(created.getId()).toUri();
        
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(location);
        return new ResponseEntity<>(created, headers, HttpStatus.CREATED);
    }
}
